backend_layout {
	colCount = 3
	rowCount = 1
	rows {
		1 {
			columns {
				1 {
					name = LLL:EXT:distribution_helper/Resources/Private/Language/locallang.xlf:backendLayout.column.left
					colPos = 0
				}
				2 {
					name = LLL:EXT:distribution_helper/Resources/Private/Language/locallang.xlf:backendLayout.column.center
					colPos = 1
				}
				3 {
					name = LLL:EXT:distribution_helper/Resources/Private/Language/locallang.xlf:backendLayout.column.right
					colPos = 2
				}
			}
		}
	}
}
