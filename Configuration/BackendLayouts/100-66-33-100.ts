backend_layout {
	colCount = 3
	rowCount = 3
	rows {
		1 {
			columns {
				1 {
					name = LLL:EXT:distribution_helper/Resources/Private/Language/locallang.xlf:backendLayout.column.header
					colspan = 3
					colPos = 2
				}
			}
		}
		2 {
			columns {
				1 {
					name = LLL:EXT:distribution_helper/Resources/Private/Language/locallang.xlf:backendLayout.column.main
					colspan = 2
					colPos = 0
				}
				2 {
					name = LLL:EXT:distribution_helper/Resources/Private/Language/locallang.xlf:backendLayout.column.aside
					colPos = 1
				}
			}
		}
		3 {
			columns {
				1 {
					name = LLL:EXT:distribution_helper/Resources/Private/Language/locallang.xlf:backendLayout.column.footer
					colspan = 3
					colPos = 3
				}
			}
		}
	}
}