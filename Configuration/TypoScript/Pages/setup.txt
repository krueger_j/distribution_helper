page {
    bodyTag >
    bodyTagCObject = COA
    bodyTagCObject {
        10 = TEXT
        10 {
            data = page:tx_distribution_helper_classes
            stdWrap.noTrimWrap = |<body class="|">
            replacement {
                10 {
                    search = #,|\.#
                    replace.char =  32
                    useRegExp = 1
                }
            }
        }
    }
}
