<?php
defined('TYPO3_MODE') or die();

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'distribution_helper',
    'Configuration/TypoScript/',
    'Distribution Helper'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'distribution_helper',
    'Configuration/TypoScript/TtContent/',
    'Wrap classes around content elements'
);

\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
	'distribution_helper',
	'Configuration/TypoScript/Pages/',
	'Generate body-Tag classes for pages'
);