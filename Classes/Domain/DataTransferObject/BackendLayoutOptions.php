<?php
namespace T3easy\DistributionHelper\Domain\DataTransferObject;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Jan Kiesewetter <jan@t3easy.de>, t3easy
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class PageTSconfig
 *
 */
class BackendLayoutOptions
{
    /**
     * @var array
     */
    protected $packages = [];

    /**
     * @var array
     */
    protected $directories = [];

    /**
     * @var array
     */
    protected $files = [];

    /**
     * @param array $pageTSconfig
     */
    public function __construct(array $pageTSconfig)
    {
        $backendLayoutOptions = [];
        if (ArrayUtility::isValidPath($pageTSconfig, 'options./backendLayout.')) {
            $backendLayoutOptions = ArrayUtility::getValueByPath($pageTSconfig, 'options./backendLayout.');
        }
        foreach ($backendLayoutOptions as $key => $value) {
            if (property_exists(__CLASS__, $key)) {
                $setter = 'set' . ucfirst($key);
                $this->$setter($value);
            } elseif (substr($key, -1) === '.') {
                $keyWithoutLastDot = substr($key, 0, -1);
                if (property_exists(__CLASS__, $keyWithoutLastDot)) {
                    $setter = 'set' . ucfirst($keyWithoutLastDot);
                    $this->$setter($value);
                }
            }
        }
    }

    /**
     * @param string $packages
     * @return void
     */
    public function setPackages($packages)
    {
        $this->packages = GeneralUtility::trimExplode(',', $packages);
    }

    /**
     * @return array
     */
    public function getPackages()
    {
        return $this->packages;
    }

    /**
     * @param array $directories
     * @return void
     */
    public function setDirectories($directories)
    {
        $this->directories = $directories;
    }

    /**
     * @return array
     */
    public function getDirectories()
    {
        return $this->directories;
    }

    /**
     * @param array $files
     * @return void
     */
    public function setFiles($files)
    {
        $this->files = $files;
    }

    /**
     * @return array
     */
    public function getFiles()
    {
        return $this->files;
    }
}
