<?php
namespace T3easy\DistributionHelper\ViewHelpers;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Jan Kiesewetter <jan@t3easy.de>, t3easy
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;

/**
 * This view helper returns its content compressed
 * It removes tabs, newlines, return and replaces multiple spaces with one space
 *
 * = Examples =
 * <code title="Inline">
 * {variable -> dh:compress()}
 * </code>
 * <output>
 *
 * </output>
 *
 * <code title="As an if condition">
 * <dh:compress>
 * 	<div>
 * 		<ul>
 * 			<li>
 * 				Level 1 item 1
 * 				<ul>
 * 					<li>Level 2 item 1</li>
 * 					<li>Level 2 item 2</li>
 * 					<li>Level 2 item 3</li>
 * 				</ul>
 * 			</li>
 * 			<li>Level 1 item 2</li>
 * 			<li>Level 1 item    3</li>
 * 		</ul>
 * 	</div>
 * </dh:compress>
 * </code>
 * <output>
 * <div><ul><li>Level 1 item 1<ul><li>Level 2 item 1</li><li>Level 2 item 2</li><li>Level 2 item 3</li></ul></li><li>Level 1 item 2</li><li>Level 1 item 3</li></ul></div>
 * </output>
 *
 */
class CompressViewHelper extends AbstractViewHelper
{
    /**
     * Return the compressed content
     *
     * @param bool $respectApplicationContext
     * @return mixed
     */
    public function render($respectApplicationContext = true)
    {
        $content = $this->renderChildren();
        $applicationContext = GeneralUtility::getApplicationContext();
        if ($respectApplicationContext === false || $applicationContext->isProduction() === true) {
            return preg_replace(
                    [
                            '/(\t|\n|\r)/',
                            '/ {2,}/',
                    ],
                    [
                            '',
                            ' ',
                    ],
                    $content
            );
        }

        return $content;
    }
}
