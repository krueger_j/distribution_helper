<?php
namespace T3easy\DistributionHelper\ViewHelpers;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Jan Kiesewetter <jan@t3easy.de>, t3easy
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class RenderViewHelper
 *
 */
class RenderViewHelper extends \TYPO3\CMS\Fluid\ViewHelpers\RenderViewHelper
{
    /**
     * @var \TYPO3\CMS\Fluid\View\StandaloneView|\TYPO3\CMS\Fluid\View\TemplateView
     */
    protected $view;

    /**
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
     * @inject
     */
    protected $configurationManager;

    /**
     * Renders the content.
     *
     * @param string $extensionName Name of the extension
     * @param string $section Name of section to render. If used in a layout, renders a section of the main content file. If used inside a standard template, renders a section of the same file.
     * @param string $partial reference to a partial
     * @param array $arguments arguments to pass to the partial
     * @param bool $optional Set to TRUE, to ignore unknown sections, so the definition of a section inside a template can be optional for a layout
     * @return string
     */
    public function render($extensionName = 'DistributionHelper', $section = null, $partial = null, $arguments = [], $optional = false)
    {
        $this->view = $this->viewHelperVariableContainer->getView();
        $arguments = $this->loadSettingsIntoArguments($arguments);

        $originalPartialRootPath = $this->view->getPartialRootPath();

        $partialRootPath = $this->getPartialRootPath($extensionName);
        $this->view->setPartialRootPath($partialRootPath);

        $content = '';
        if ($partial !== null) {
            $content = $this->viewHelperVariableContainer->getView()->renderPartial($partial, $section, $arguments);
        } elseif ($section !== null) {
            $content = $this->viewHelperVariableContainer->getView()->renderSection($section, $arguments, $optional);
        }

        $this->view->setPartialRootPath($originalPartialRootPath);

        return $content;
    }

    /**
     * @param $extensionName
     * @return string
     */
    protected function getPartialRootPath($extensionName)
    {
        $configuration = $this->configurationManager->getConfiguration(
            \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_FRAMEWORK,
            $extensionName
        );
        if (isset($configuration['view']['partialRootPath']) && strlen($configuration['view']['partialRootPath']) > 0) {
            return GeneralUtility::getFileAbsFileName(
                $configuration['view']['partialRootPath']
            );
        }

        return str_replace(
                '@packageResourcesPath',
                ExtensionManagementUtility::extPath(
                    GeneralUtility::camelCaseToLowerCaseUnderscored($extensionName)
                ) . 'Resources',
                '@packageResourcesPath/Private/Partials');
    }
}
