﻿.. ==================================================
.. FOR YOUR INFORMATION
.. --------------------------------------------------
.. -*- coding: utf-8 -*- with BOM.

.. include:: ../Includes.txt


.. _developer-manual:

Developer manual
================

Backend Layouts from files
--------------------------

To load your backend layout files automatically from your extension:

#. Put your backend layout files in */Configuration/BackendLayouts/*
#. Set the category to *distribution*
#. Place the icons for the layouts in */Resources/Public/Icons/BackendLayouts/* the must have the same filename as
   the backend layout file.
#. The filename is used as description and part of the identifier. You can set a more user friendly description for
   the backend layout in */Resources/Private/Language/locallang.xlf* (or locallang.xml).
#. The layouts now appear in the page configuration tab appearance

.. figure:: ../Images/PageAppearance.png
	:width: 850
	:alt: Page appearance tab

	The page appearance tab

Classes for content elements and pages
--------------------------------------

By default, no classes are shipped with the extension.
To add classes to the list you must add them with Page TSConfig.
If you want to add one item for multiple classes, separate them with an escaped dot :typoscript:`\.`.
To enable the wrapping around content elements you have to add the static TypoScript `Wrap classes around content elements`
to you template or implement an own solution.

Example Page TSConfig:

::

    TCEFORM.tt_content.tx_distribution_helper_classes {
      addItems.hello = hello
      addItems.whats_up = What's up?
      addItems.multiple\.classes = Multiple classes
    }
    TCEFORM.pages.tx_distribution_helper_classes {
      addItems.hello = hello
      addItems.whats_up = What's up?
      addItems.multiple\.classes = Multiple classes
    }

